-- take a ttt file and turn it into a table
dbg = function() return end

function digest(line)
	local mytab = {}
	for t in string.gmatch(line.."\t", "(.-)\t") do
		table.insert(mytab,t)
	end
	return mytab
end

function checkin(tab, line)
	local crumbs = digest(line)
	tab.unmatched={stamp=crumbs[1], name=crumbs[2], label=crumbs[3], comment=crumbs[4]}
end

function checkout(tab, line)
	local crumbs = digest(line)
	tab.slots = tab.slots or {}
	table.insert(tab.slots, {["in"]=tab.unmatched.stamp,out=crumbs[1], name=tab.unmatched.name, label=tab.unmatched.label, comment=tab.unmatched.comment})
	tab.unmatched=nil
end

function ttt(str)

	local timetable = {}
	local curentry = "?"
	local checkedin = false

	for l in string.gmatch(str.."\n", "(.-)\n") do
		if string.sub(l,1,1) == "@" then
dbg("NEW DAY")
			curentry = string.sub(l,2,-1)
		elseif string.match(l,"^%d") then
dbg("CONFORMING LINE:",l)
			if checkedin then
dbg("CHECKED OUT")
				checkout(timetable[curentry], l)
				checkedin = false
			else
dbg("CHECKED IN")
				timetable[curentry] = timetable[curentry] or {}
				checkin(timetable[curentry], l)
				checkedin = true
			end
		else
dbg("NON-CONFORMING LINE:",l)
		end
	end
	return timetable
end

-- DEBUG

--[[
function prettyprint(tab, ident)
	ident = ident or ""

	for k, v in pairs(tab) do
		io.write(ident)
		io.write(k)
		io.write(": ")
		if type(v) == "table" then
			io.write("\n-")
			io.write(prettyprint(v,ident.."\t"))
		else
			io.write(v)
		end
		io.write("\n")
	end

end

prettyprint(ttt(io.read("*all")))
--]]


function totime(tstr, date)
print(date)
	date = os.date("*t", (date or os.time()))
	-- create a time object
	-- parse the time. It should be in the form HH:MM
	hour, minute = string.match(tstr, "(%d-):(%d%d)")
	return os.time({day = date.day, month=date.month, year=date.year, hour=tonumber(hour), min=tonumber(minute)})
end

function todate(date)
	m,d,y = string.match(date, "(%d-)/(%d-)/(%d*)")
	return os.time({day = tonumber(d), month = tonumber(m), year = tonumber(y)})
end

function interval(h1,m1,h2,m2)
	-- if h2 < m2 we add 12 hours to h2
	if h1 > h2 then
		h2 = h2 + 12
	end
	
	-- now multiply hours by 60
	time1 = (h1*60)+m1
	time2 = (h2*60)+m2
	delta = time2-time1
	dh = math.floor(delta/60)
	dm = delta-(dh*60)
	return string.format("%d:%02d",dh,dm)
end

function hours(h)
	hour, minute = string.match(h, "(%d-):(%d%d)")
	return tonumber(hour)
end

function minutes(m)
	hour, minute = string.match(m, "(%d-):(%d%d)")
	return tonumber(minute)
end

function htom(ts)
	return hours(ts)*60+minutes(ts)
end
