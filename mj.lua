require "tttparser"

-- USAGE: mj <hours|card> <file.ttt>

f, err = io.open(arg[2],"r")

if err then
	print(err)
	os.exit(1)
end

thetab = ttt(f:read("*all"))
f:close()

if arg[1] == "hours" then
	for date, chart in pairs(thetab) do
		io.write("HOURS FOR ")
		io.write(date)
		io.write("\t")

		tally=0
		for i, slot in ipairs(chart.slots) do
print("DEBUG:",interval(hours(slot['in']),minutes(slot['in']), hours(slot.out), minutes(slot.out)))

			tally = tally+htom(interval(hours(slot['in']),minutes(slot['in']), hours(slot.out), minutes(slot.out)))
		end
		th = math.floor(tally/60)
		tm = tally-th
		print(th..":"..tm)
	end
end
